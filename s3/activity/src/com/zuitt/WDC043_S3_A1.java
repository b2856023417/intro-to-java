package com.zuitt;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed:");
        Scanner in = new Scanner(System.in);
        int num = 0;
        try {
            num = in.nextInt();
        }

        catch (Exception e) {
            System.out.println("Invalid input!");
            e.printStackTrace();
            System.exit(0);
        }

        int checkInput = 1;
        if (num == 0) {
            checkInput = 0;
        } else if (num <= 0) {
            checkInput = -1;
        }

        switch(checkInput) {
            case 0:
                System.out.println("The factorial of 0 is 1.");
                break;
            case 1:
                int answerWhile = 1;
                int counter = 1;
                int numWhile = num;

                while(numWhile >= counter) {
                    answerWhile *= numWhile;
                    numWhile--;
                }

                System.out.println("(While Loop) The factorial of " + num + " is " + answerWhile + ".");

                int answerFor = 1;

                for (int numFor = num; numFor >= 1; numFor--) {
                    answerFor *= numFor;
                }

                System.out.println("(For Loop) The factorial of " + num + " is " + answerFor + ".");

                break;
            case -1:
                System.out.println("Number cannot be negative!");
                break;

            default:
                System.out.println();
        }
    }
}

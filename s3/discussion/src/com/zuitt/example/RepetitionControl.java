package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    // Loops
        // are control structures that allow code blocks to be executed multiple times
    public static void main(String[] args) {
        // While Loops
            // Allows for repetitive use of code, similar to for loops, but are usually for situations where the content to iterate through is indefinite

        int x = 0;

        while (x < 10) {
            System.out.println("(While Loop) Loop Number: " + x);
            x++; // increment/decrement of iteration
        }

        // Do-while
        // Similar to while loops
        // However, do-while loops will always execute at least once. On the other hand, while loops may not execute at all

        int y = 10;
        do {
            System.out.println("(This will run ar least once) Countdown " + y);
            y--;
        }
        while (y > 10);

        // For Loops
        // Syntax
        /*
            for (initialVal; condition; iteration) {
                // Code block
            }
        * */
        // Mini-Activity:
        for (int i = 0; i < 10; i++) {
            System.out.println("(For Loop) Current Count: " + i);
        }

        // For Loops with Arrays
        int[] intArray = {100, 200, 300, 400, 500};
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(intArray[i]);
        }

        // For-each Loops with Arrays
        /*
            Syntax:
                for(dataType itemName: arrayName) {
                    // Code block
                }
        * */

        // Use cases: If you don't know the exact number of elements in an array
        String[] boyBandArray = {"John", "Paul", "George", "Ringo"};
        for (String member: boyBandArray) {
            System.out.println(member);
        }

        // Multi-dimensional Arrays Loops
        String[][] classroom = new String[3][3];
        //[row][column]

        //First row
        classroom[0][0] = "Jennie";
        classroom[0][1] = "Lisa";
        classroom[0][2] = "Rose";
        //Second row
        classroom[1][0] = "Ash";
        classroom[1][1] = "Misty";
        classroom[1][2] = "Brock";
        //Third row
        classroom[2][0] = "Amy";
        classroom[2][1] = "Lulu";
        classroom[2][2] = "Morgan";

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.println("classroom[" + row + "][" + col + "] = " + classroom[row][col] );
            }
        }

        for (String[] row: classroom) {
            for (String column: row) {
                System.out.println(column); // Prints out individual elements
            }
        }

        // Print as a Matrix
        for (int row = 0; row < classroom.length; row++) {
            for (int col = 0; col < classroom[row].length; col++) {
                System.out.print(classroom[row][col] + " ");
            }
            System.out.println();
        }

        System.out.println();

        // Mini Activity 2
        String[][] sampleMatrix = new String[3][3];

        // First Row
        sampleMatrix[0][0] = "*";
        sampleMatrix[0][1] = "*";
        sampleMatrix[0][2] = "*";

        // Second Row
        sampleMatrix[1][0] = "2";
        sampleMatrix[1][1] = "8";
        sampleMatrix[1][2] = "5";

        //Third Row
        sampleMatrix[2][0] = "*";
        sampleMatrix[2][1] = "*";
        sampleMatrix[2][2] = "*";

        for (int row = 0; row < sampleMatrix.length; row++) {
            for (int col = 0; col < sampleMatrix[row].length; col++) {
                System.out.print(sampleMatrix[row][col] + " ");
            }
            System.out.println();
        }

        // For-each Loops with ArrayList
        // Syntax
            /*
                arrayListName.foreach(Consumer<E> -> // Code block);
                // "->" This is called the lambda operator - is used to separate parameter and implementation

            * */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("ArrayList: " + numbers);

        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        // For-each Loops with HashMaps
        // Syntax
            /*
            * hashMapName.foreach((key, value) -> // Code block);
            *
            * */

        HashMap<String, Integer> grades = new HashMap<String, Integer>() {
            {
                this.put("English", 90);
                this.put("Math", 85);
                this.put("Science", 97);
                this.put("History", 94);
            }
        };

        grades.forEach((subject, grade) -> {
            System.out.println("HashMaps: " + subject + " : " + grade + "\n");
        });
    }
}

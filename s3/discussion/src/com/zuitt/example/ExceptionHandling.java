package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {

    // Exceptions
    // Problem that arises during the execution of a program
    // This disrupts the normal flow of the program and terminates it abnormally

    // Exception Handling
    // Refers to managing and catching run-time errors in order to safely run your code

    // Errors encountered in Java
        // Compile-time errors - errors that usually happen when you try to compile a program that is syntactically incorrect or has missing package imports
            // There is an error and is not able to run
        // Run-time error - errors that happen after the compilation and during the execution of the program
            // Common example: User gives a String instead of a number in forms
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Please enter a number: ");

        // Try
        // Try to execute the statement
        // We expect the user to input a number

        try {
            num = input.nextInt();
        }

        // Catch
        // Catch any errors
        // Exception class represented by an 'e'
        // The class 'Exception' and its subclasses are a form of Throwable that indicates conditions that a reasonable application might want to catch

        catch (Exception e) {
            System.out.println("Invalid input!");
            // Prints throwable error along with other details like the line number, class name where the exception occurred

            e.printStackTrace();
        }

        // Optional block - it will execute whether there are any errors encountered or not

        finally {
            System.out.println("I will run no matter what happens! You have entered: " + num);
        }


    }

}

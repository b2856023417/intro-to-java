package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    // Java Collection
    // Are a single unit of objects
    // Useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops

    public static void main(String[] args) {
        // Array
            // In Java, arrays are container of values of the same type given a predefined amount of values
            // Java arrays are more rigid, once the size and data type are defined, they can no longer be changed

        // Array Declaration
            // DataType[] identifier = new dataType[numOfElements];
            // "[]" indicates that data type should be able to hold multiple values
            // The "new" keyword is used for non-primitive data types to tell Java to create the said variable
            // [numOfElements] - number of elements

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;
        // intArray[5] = 100; // Error: out of bounds

        System.out.println(intArray); // output: [I@5594a1b5 (Memory Address)

        // To print the intArray, we need to import the Array Class and use the .toString() Method

        System.out.println(Arrays.toString(intArray));

        // Array Declaration with Initialization
            // DataType[] identifier = {elementA, elementB, elementC, ...}
            // The compiler automatically specifies the size by counting the number of elements in the array
        String[] names = {"John", "Jane", "Joe"};
        // names[3] = "Joey";

        System.out.println(Arrays.toString(names));

        // Sample Java Array Methods
        // Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort():");
        System.out.println(Arrays.toString(intArray));

        // Multi-dimensional Array
        // A Two-dimensional array, can be described by two lengths nested within each other, like a matrix
        // First length is "row", second length is "column"

        String[][] classroom = new String[3][3];
        // First Row
        classroom[0][0] = "Naruto";
        classroom[0][1] = "Sasuke";
        classroom[0][2] = "Sakura";

        // Second Row
        classroom[1][0] = "Linny";
        classroom[1][1] = "Tuck";
        classroom[1][2] = "Ming-ming";

        // Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.toString(classroom)); // output: [[Ljava.lang.String;@3b6eb2ec, [Ljava.lang.String;@1e643faf, [Ljava.lang.String;@6e8dacdf]

        // For multi-dimensional arrays
        System.out.println(Arrays.deepToString(classroom));

        // ArrayLists
            // Are resizable arrays, wherein elements can be added or removed whenever it is needed
        // Syntax
            // ArrayList<T> identifier = new ArrayList<T>();
            // '<T>' is used to specify that the list can only have one type of objects in a collection
        // ArrayList cannot hold primitive data types, 'Java Wrapper Classes' provide a way to use these types as objects
        // Java Wrapper Classes - object version of primitive data types with methods

        // ArrayList Declaration
        // ArrayList<int> numbers = new ArrayList<int>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        ArrayList<String> students = new ArrayList<String>();

        // Adding Elements
        // arrayListName.add(element);
        students.add("Cardo");
        students.add("Luffy");

        System.out.println(students);

        // Declare an ArrayList with values
        ArrayList<String> students2 = new ArrayList<>(Arrays.asList("Jane", "Mike"));

        System.out.println(students2);

        // Accessing Elements
        // arrayListName.get(index);
        System.out.println(students2.get(0));

        // Adding Elements on a Specific Index
        // ArrayListName.add(index, element);
        students2.add(0, "Cardi"); // Cardi
        System.out.println(students2.get(0)); // [Cardi, Jane, Mike]
        System.out.println(students2);

        // Updating an Element
        // arrayListName.set(index, element)
        students2.set(1, "Tom");
        System.out.println(students2);

        // Removing an Element
        // arrayListName.remove(index);
        students2.remove(1);
        System.out.println(students2);

        // Removing All Elements
        students2.clear(); // Or students2.removeAll(students2)
        System.out.println(students2);

        // Getting the arrayList size
        System.out.println(students2.size());

        // HashMaps
            // Most objects in Java are defined and are instantiations of classes that contain a proper set of properties and methods
            // There might be use cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs
            // In Java, 'keys' are also referred as "fields"
            // Wherein the values are accessed by the fields

            // Syntax
                // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();

        // HashMap Declaration
        // HashMap<String, String> jobPosition = new HashMap<String, String>();

        // HashMap Declaration with Initialization
        HashMap<String, String> jobPosition = new HashMap<>() {
            {
                put("Teacher", "Cee");
                put("Web Developer", "Peter Parker");
            }
        };

        // Adding Elements
        // hashMapName.put(<fieldName>, <value>);
        jobPosition.put("Dreamer", "Morpheus");
        jobPosition.put("Police", "Cardo");
        System.out.println(jobPosition);

        // Accessing Elements
        // hashMapName.get("fieldName");
        System.out.println(jobPosition.get(0));

        // Updating Values
        // hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Dreamer", "Persephone");
        System.out.println(jobPosition);

        // Removing Elements
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        // Retrieving HashMap Keys
        System.out.println(jobPosition.keySet());

        // Retrieving HashMap Values
        System.out.println(jobPosition.values());
    }
}

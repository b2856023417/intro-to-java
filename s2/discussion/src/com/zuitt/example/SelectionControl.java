package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {

    public static void main(String[] args) {

        // Java Operators
        // Arithmetic Operators
            // +, -, *, /, %
        // Comparison Operators
            // >, <, >=, <=, ==, !=
        // Logical Operators
            // &&, ||, !
        // Assignment Operators
            // =, +=

        // Control Structures in Java
        // If-Else Statements
            // Statement allows us to manipulate the flow of code depending on the evaluation of the condition
        // Syntax:
        /*
            if(condition) {
                // Code Block
            } else {
                // Code Block
            }
        */

        // Mini Activity
        // Create a Divisibility checker using if and else

        /*Scanner userInput = new Scanner(System.in);
        System.out.println("Enter a number to be divided by 5:");
        int number = userInput.nextInt();

        if ((number % 5) == 0) {
            System.out.println("It is divisible.");
        } else {
            System.out.println("It is not divisible.");
        }*/

        // Short-circuiting
        // A technique that is applicable only to the AND & OR operators wherein if statements or other control structures can exit early by ensuring safety of operation or efficiency
        // Right hand operand is not evaluated
        // OR Operator
            // (true || ...) = true
        // AND Operator
            // (false && ...) = false

        int x = 15;
        int y = 0;

        if(y != 0 && x/y == 0) {
            System.out.println("Result is: " + x/y);
        } else {
            System.out.println("This will only run because of short circuiting!");
        }

        // Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

        // Switch Cases

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int directionValue = numberScanner.nextInt();

        switch (directionValue) {

            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");

        }

    }
}

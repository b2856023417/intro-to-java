package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity {
    public static void main(String[] args) {
        int[] primeNumbers = {2, 3, 5, 7, 11};
        System.out.println("The first prime number is: " + primeNumbers[0]);
        System.out.println("The second prime number is: " + primeNumbers[1]);
        System.out.println("The third prime number is: " + primeNumbers[2]);
        System.out.println("The fourth prime number is: " + primeNumbers[3]);
        System.out.println("The fifth prime number is: " + primeNumbers[4]);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane"));
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);
    }
}


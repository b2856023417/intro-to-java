package com.zuitt;

import java.util.Scanner;
public class LeapYear {
    public static void main(String[] args) {
        Scanner userInputYear = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year:");
        int year = userInputYear.nextInt();

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.println("Year " + year + " is a leap year.");
                } else {
                    System.out.println("Year " + year + " is not a leap year.");
                }
            } else {
                System.out.println("Year " + year + " is a leap year.");
            }
        } else {
            System.out.println("Year " + year + " is not a leap year.");
        }
    }
}

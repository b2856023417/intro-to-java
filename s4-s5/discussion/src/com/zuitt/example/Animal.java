package com.zuitt.example;
// Animal class will serve as the parent class
public class Animal {
    // Properties
    private String name;
    private String color;

    // Constructor
    public Animal(){}

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    // Setter and Getter
    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // Method
    public void call() {
        System.out.println("Hi, my name is " + this.name + "! (From Animal.java)");
    }
}

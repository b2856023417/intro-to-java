package com.zuitt.example;

public class Driver {
    // It is another component or composition that is needed by another class

    // Properties
    private String name;

    // Constructors
    public Driver(){}
    public Driver(String name) {
        this.name = name;
    }

    // Getter and Setter
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }


}

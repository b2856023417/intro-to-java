package com.zuitt.example;

public class Child extends Parent{
    public void speak() {
        // Original method from the Parent class
        super.speak();
        System.out.println("I am a child!");
    }
}

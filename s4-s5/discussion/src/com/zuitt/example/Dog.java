package com.zuitt.example;
// Child class of Animal class
// 'extends' - is used to inherit the properties and methods of the parent class
public class Dog extends Animal {
    // Properties
    private String breed;

    // Constructor
    public Dog() {
        // 'super()' - direct access with the original constructor - to inherit
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    // Getter and Setter
    public String getBreed() {
        return this.breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    // Method
    public void speak() {
        System.out.println("Woof woof!");
    }
    public void call() {
        super.call(); // Direct access to the parent 'call' method
        System.out.println("Hi, my name is " + this.getName() + "! (From Dog.java)");
    }

}

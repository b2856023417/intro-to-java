package com.zuitt.example;

public class Person implements Actions, Greetings{
    public void sleep() {
        System.out.println("Zzz... (snore)");
    }
    public void run() {
        System.out.println("Running!");
    }

    // Mini Activity
    // Create a Greetings Interface morningGreet() and holidayGreet()
    // Implement it in Person.java

    public void morningGreet() {
        System.out.println("Good morning!");
    }
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }

}

import com.zuitt.example.*;
// import com.zuitt.example.*; (To import all)

public class Main {
    public static void main(String[] args) {

        // OOP
        // OOP stands for Object-Oriented Programming
        // OOP is a programming model that allows developers to design software around data or objects rather than function and logic

        // OOP Concepts
            // Object - abstract idea that represents something in the real world
            // Example: the concept of a dog
            // Class - representation of the object using code
            // Example: Writing a code that would describe a dog
            // Instance - unique copy of the idea, made 'physical'
                // Actual creation or the physical copy
                // Enables us to copy the concept such as an instance
            // Example: Instantiating a dog named 'Fluffy' from the dog class

        // Objects
            // "States and Attributes" - what is the idea about?
            // Properties of the specific object
            // Dog's name, breed, color, etc.
            // Behaviors - what can the idea do?
                // What can the actual dog do?
                // bark, sit, etc.

        // Four Pillars of OOP
            // 1. Encapsulation

            // A mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit
            // "Data hiding" - the variables of a class will be hidden from the other classes, and can be accessed only through the methods of their current class
            // To achieve encapsulation, these are some examples:
                // Variable/Properties as Private
                // Provide a public Setter and Getter function

        // Create a Car

        Car myCar = new Car(); // We accessed the empty/default constructor
        myCar.drive();

        // Assigning the properties to 'myCar' using setter methods

        myCar.setName("Hi-Ace");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2023);

        // myCar.name; (Doesn't work because of 'private' encapsulation)

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".");

        // Mini Activity - instantiate another car from the car class

        Car mySecondCar = new Car("Enzo", "Ferrari", 2002);

        System.out.println("My other car which is a " + mySecondCar.getBrand() + " " + mySecondCar.getName() + " was made in " + mySecondCar.getYearOfMake() + ".");

        // Composition and Inheritance

        // Inheritance - allows modelling an object that is a subset of another object
            // It defines 'is a' relationship
        // Composition - allows modelling objects that are made up of other objects
            // Both entities are dependent on each other
            // Composed object cannot exist without the other entity
            // It defines 'has a' relationship

        // Example
            // A car is a vehicle - inheritance
            // A car has a driver - composition

        myCar.setDriver("Dodong");

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        // 2. Inheritance
        // Can be defined as the process where one class acquire the properties and methods of another class
        // With the use of inheritance, the information is made manageable in hierarchical order

        Dog myPet = new Dog();
        myPet.setName("Bantay");
        myPet.setColor("Brown");
        myPet.speak(); // Woof woof!
        System.out.println("My dog's name is " + myPet.getName() + " and it is a " + myPet.getColor() + " " + myPet.getBreed() + "!");

        myPet.call();

        // 3. Abstraction - is a process where all logic and complexity are hidden from the user

            // Interfaces
                // This is used to achieve total abstraction
                // Creating abstract classes doesn't support "multiple inheritance" but it can be achieved with interfaces
                // Interface allows multiple implementation, list functionalities
                // Act as "contracts" wherein a class implements the interface which should have the methods that are defined in the class

        Person child = new Person();
        child.sleep();
        child.run();
        child.morningGreet();
        child.holidayGreet();

        // 4. Polymorphism
            // This is usually done by function/method overloading

            // 2 Main Types of Polymorphism
            // A. Static or compile-time polymorphism
                // Methods with the same name, but they have different data types and a different number of arguments

        StaticPoly myAddition = new StaticPoly();

        // Original method
        System.out.println(myAddition.addition(5, 6));
        // Based on arguments
        System.out.println(myAddition.addition(5, 6, 7));
        // Based on data types
        System.out.println(myAddition.addition(5.7, 6.6));

            // B. Dynamic or run-time polymorphism
                // Function is overridden by replacing the definition of the method in the parent class in the child class

        Child myChild = new Child();
        myChild.speak();

        // System.out.println("Hello world!");
    }
}
package com.zuitt;

public class Contact extends Phonebook implements Queries {
    // Variables
    private String name;
    private String contactNumber1;
    private String contactNumber2;
    private String homeAddress;
    private String officeAddress;


    // Constructor
    public Contact() {
        this.contactNumber2 = "N/A";
    }

    public Contact(String name, String contactNumber1, String contactNumber2, String homeAddress, String officeAddress) {
        this.name = name;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.homeAddress = homeAddress;
        this.officeAddress = officeAddress;
    }


    // Getter and Setter
    public String getName() {
         return this.name;
    }

    public String getContactNumber1() {
        return this.contactNumber1;
    }

    public String getContactNumber2() {
        return this.contactNumber2;
    }

    public String getHomeAddress() {
        return this.homeAddress;
    }

    public String getOfficeAddress() {
        return this.officeAddress;
    }

    public void getAllInfo() {
        System.out.println(name + "\n--------------------\n" + name + " has the following registered numbers:\n" + contactNumber1 + "\n" + contactNumber2 + "\n------------------------------\n" + name + " has the following registered addresses:\nMy home is in " + homeAddress + "\n" + "My office is in " + officeAddress + "\n====================");
    }
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber1(String contactNumber1) {
        if(contactNumber1.length() == 11) {
            this.contactNumber1 = contactNumber1;
        }

    }

    public void setContactNumber2(String contactNumber2) {
        if(contactNumber2.length() == 11) {
            this.contactNumber2 = contactNumber2;
        }
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

}

package com.zuitt;

import java.util.ArrayList;

public class Phonebook implements Queries{
    // Variables
    private ArrayList<Contact> phoneBook = new ArrayList<Contact>();

    // Constructor
    public Phonebook(){};

    public Phonebook(ArrayList<Contact> phoneBook) {
        this.phoneBook = phoneBook;
    }

    // Getter and Setter
    public void getAllInfo() {
        if (phoneBook.size() == 0) {
            System.out.println("Phonebook is empty!");
        } else {
            phoneBook.forEach(contact -> System.out.println(contact.getName() + "\n--------------------\n" + contact.getName() + " has the following registered numbers:\n" + contact.getContactNumber1() + "\n" + contact.getContactNumber2() + "\n------------------------------\n" + contact.getName() + " has the following registered addresses:\nMy home is in " + contact.getHomeAddress() + "\n" + "My office is in " + contact.getOfficeAddress() + "\n===================="));
        }
    }

    public void setAddContact(Contact contact) {
        phoneBook.add(contact);
    }

}

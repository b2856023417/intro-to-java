import com.zuitt.Contact;
import com.zuitt.Phonebook;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        String userSetName;
        String userSetContactNumber1;
        String userSetContactNumber2;
        String userSetHomeAddress;
        String userSetOfficeAddress;

        Scanner userInput = new Scanner(System.in);
        System.out.println("Do you want to add contact details to the phonebook? (1/0)");
        Integer answer1 = userInput.nextInt();
        userInput.nextLine();
        while (answer1 == 1) {
            Contact contact = new Contact();

            System.out.println("Please enter their first name and last name:");
            userSetName = userInput.nextLine();
            contact.setName(userSetName);

            System.out.println("Please enter their contact number:");
            userSetContactNumber1 = userInput.nextLine();
            contact.setContactNumber1(userSetContactNumber1);

            System.out.println("Do they have another contact number? (1/0)");
            Integer answer2 = userInput.nextInt();
            userInput.nextLine();
            if (answer2 == 1) {
                System.out.println("Please enter their other contact number:");
                userSetContactNumber2 = userInput.nextLine();
                contact.setContactNumber2(userSetContactNumber2);
            }
            System.out.println("Please enter their home address:");
            userSetHomeAddress = userInput.nextLine();
            contact.setHomeAddress(userSetHomeAddress);

            System.out.println("Please enter their office address:");
            userSetOfficeAddress = userInput.nextLine();
            contact.setOfficeAddress(userSetOfficeAddress);


            System.out.println("Do you want to add contact details to the phonebook? (1/0)");
            phonebook.setAddContact(contact);
            answer1 = userInput.nextInt();
            userInput.nextLine();
        }

        if (answer1 == 0) {
            System.out.println("Do you want to view the phonebook? (1/0)");
            Integer answer3 = userInput.nextInt();
            if (answer3 == 1) {
                phonebook.getAllInfo();
            } else {
                System.exit(0);
            }
        }

        // System.out.println("Hello world!");
    }
}
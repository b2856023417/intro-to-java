package com.zuitt.example;
import java.util.Scanner;
public class Grades {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name");
        firstName = userInput.nextLine();

        System.out.println("Last Name");
        lastName = userInput.nextLine();

        System.out.println("First Subject Grade");
        firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade");
        secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade");
        thirdSubject = userInput.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);


    }
}

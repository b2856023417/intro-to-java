// Main class
    // Entry point of our Java program
    // Main class has 1 method inside, the 'main' method => to run our code
    //  It is where running and execution happens
    // public - access modifier which simply tells the app which classes have access to method/attributes

public class Main {
    // static - keyword associated with a method/property that is related in a class. This will allow a method to be invoked without instantiating
    // void - keyword that is used to specify a method doesn't return anything. In Java, we have to declare the data type of the method's return
    public static void main(String[] args) {
        // To print statement in the terminal
        System.out.println("Hello world!");
    }
}
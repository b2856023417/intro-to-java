// Packages in Java is used to group related classes
// Think of it as a folder in a file directory
    // Packages are divided into 2 categories:
    // 1. Built-in Packages (Packages from Java API)
    // 2. User-defined Packages (Create your own Package)

// Reversed Domain Name Notation
package com.zuitt.example;

public class Variables {
    // For the meantime, we will run within the class file
    public static void main(String[] args) {

        // Naming Conventions
        // The terminology used for variable names is "identifier"
        // Syntax: dataType identifier;

        // Variable
        int age;
        char middleName;

        // Declaration
        int x;
        // Declaration with Initialization
        int y = 1;

        // Initialization after Declaration
        x = 1;

        // Change Values
        y = 2;

        //
        System.out.println("The value of x is " + x + " and the value of y is " + y + ".");

        // Primitive Data Types
        // Pre-defined within the Java programming language which is used for single-valued variables with limited capabilities

        // Int - Whole Number Values
        int wholeNumber = 1000;
        System.out.println(wholeNumber);

        // Long
        // L is added to the end of the long numbers to be recognized
        long worldPopulation = 8045311488L;

        // Float
        // Add F at the end of a float
        // 7 Decimal places
        float piFloat = 3.14159265359F;
        System.out.println(piFloat);

        // Double - floating point values
        // 16 Decimal places
        double piDouble = 3.141592653589793238;
        System.out.println(piDouble);

        // Char - Single characters
        char letter = 'c';
        System.out.println(letter);

        // Boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // Constant
        // Final - keyboard
        // Common Practice - CAPITAL LETTERS for readability
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        // PRINCIPAL = 4000;

        // Non-primitive Data Types
        // Can store multiple values
        // AKA as reference data types - refer to instances or objects
        // Do not directly store the value of a variable, but rather remembers the reference to the variable

        // String
        // Stores a sequence or array of characters

        String userName = "CardoD";
        System.out.println(userName);

        //Sample String Methods
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
